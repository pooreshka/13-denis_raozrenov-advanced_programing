#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>

int main()
{
    WSAInitializer wsaInit;
    Server *serv = new Server();
    while (true) {
        serv->serve();
    }
}