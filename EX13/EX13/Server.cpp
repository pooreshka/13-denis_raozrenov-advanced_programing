#include "Server.h"
#include "Helper.h"
#include <exception>
#include <thread>
#include <thread>
#include <mutex>
#include <fstream>
#include <string.h>

#define PORT 8826
#define INT_LENGTH 2
#define LOGIN 200
#define CLIENT_UPDATE 204
#define ERROR 800

std::mutex userSet; // protects set list of users

Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve()
{
	
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << PORT << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		acceptClient();
	}
}

std::string Server::getAllUsers()
{
	std::string allUsers;

	for (auto it = this->_allUsers.begin(); it !=
		this->_allUsers.end(); ++it)
		allUsers += "&" + *it;

	return allUsers;
}

void Server::acceptClient()
{

	// this accepts the client and create a specific socket from server to this client
	// the process will not continue until a client connects to the server
	SOCKET client_socket = accept(_serverSocket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;
	// the function that handle the conversation with the client
	std::thread accept(&Server::clientHandler, this, client_socket);
	accept.detach();
}


void Server::clientHandler(SOCKET clientSocket)
{
	std::string userName;
	bool exit = false;
	while (exit != true) {
		//login variables
		int typeCode, intPart;
		std::string allUsers;

		//Client update variables
		int lenSecondUser, lenNewMessage;
		std::string secondUsername, newMessage, fileName, chatContent;
		std::ofstream chatFile;
		std::ifstream chat_file;

		typeCode = Helper::getMessageTypeCode(clientSocket);
		if (typeCode == LOGIN) {

			intPart = Helper::getIntPartFromSocket(clientSocket, INT_LENGTH);
			userName = Helper::getStringPartFromSocket(clientSocket, intPart);

			std::cout << "userName is - " << userName << std::endl;

			const std::lock_guard<std::mutex> lock(userSet);
			this->_allUsers.insert(userName);

			allUsers = getAllUsers();

			allUsers.erase(0, 1); // removes first character (that is '&')
			Helper::send_update_message_to_client(clientSocket, "", "", allUsers);
			allUsers.clear();
		}
		else if (typeCode == CLIENT_UPDATE) {
			lenSecondUser = Helper::getIntPartFromSocket(clientSocket, 2);
			secondUsername = Helper::getStringPartFromSocket(clientSocket, lenSecondUser);
			lenNewMessage = Helper::getIntPartFromSocket(clientSocket, 5);
			newMessage = Helper::getStringPartFromSocket(clientSocket, lenNewMessage);

			allUsers = getAllUsers();

			if (lenSecondUser == 0) {
				Helper::send_update_message_to_client(clientSocket, "", "", allUsers);
			}
			else {

				//sort usernames for files
				if (userName.compare(secondUsername) < 0)
				{
					fileName.append(userName);
					fileName.append("&");
					fileName.append(secondUsername);
					fileName.append(".txt");
				}
				else if (userName.compare(secondUsername) > 0) {
					fileName.append(secondUsername);
					fileName.append("&");
					fileName.append(userName);
					fileName.append(".txt");
				}

				//open chat file
				chatFile.open(fileName, std::ofstream::app);
				if (!(newMessage == "")) {
					chatFile << "&MAGSH_MESSAGE&&Author&" << userName << "&DATA&" << newMessage;
				}
				chatFile.close();

				allUsers = getAllUsers();

				//reading chat file
				chat_file.open(fileName);
				std::getline(chat_file, chatContent);
				chat_file.close();

				//sending update to user
				Helper::send_update_message_to_client(clientSocket, chatContent, secondUsername, allUsers);
			}
		}
		else if (typeCode == ERROR) {
			//user disconnected
			const std::lock_guard<std::mutex> lock(userSet);
			this->_allUsers.erase(userName);
			exit = true;
		}
	}
}
