#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <set>
#include <iostream>
#include <string>


class Server
{
public:
	Server();
	~Server();
	void serve();
	std::string getAllUsers();

private:

	void acceptClient();
	void clientHandler(SOCKET clientSocket);


	std::set<std::string> _allUsers;
	SOCKET _serverSocket;
};

